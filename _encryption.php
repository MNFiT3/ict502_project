<?php
    
    //$tmp = dec_enc('encrypt', 'hello', '1253');
    //echo $tmp;
    //echo '<br />';
    //echo dec_enc('decrypt', $tmp, '');

    function dec_enc($action, $string, $salt) {
        include "_config.php";
        $output = false;
    
        $encrypt_method = "AES-256-CBC";
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
    
        if( $action == 'encrypt' ) {
            $string = $string.' '.$salt;
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            return base64_encode($output);
        }else if( $action == 'decrypt' ){
            return openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }else{
            return null;
        }
    }
?>