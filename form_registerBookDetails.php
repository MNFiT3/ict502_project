<?php 
    include_once "_p1.php";
    include_once "_session.php";
    isLoggedIn();
?>
<html>
    <head>
        <title>📚 </title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div>
            <?php include "navbarManager.php"; ?>
            <div style="padding: 100px">
                <form>
                    <div class="form-goup">
                        <label>Author Name</label>
                        <div class="row">
                            <div class="col"><input id="newAuthor" type="text" class="form-control"></div>
                            <div class="col-sm-">OR</div>
                        </div>
                    </div>
                    <div class="form-goup">
                        <label>Genre</label>
                        <div class="row">
                            <div class="col"><input id="newGenre" type="text" class="form-control"></div>
                            <div class="col-sm-">OR</div>
                        </div>
                    </div>
                    <div class="form-goup">
                        <label>Status</label>
                        <div class="row">
                            <div class="col"><input id="newStatus" type="text" class="form-control"></div>
                            <div class="col-sm-">OR</div>
                        </div>
                    </div><br />
                    <button id="btn_add" type="button" class="btn">Add</button>
                </form>
            </div>
        </div>
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                $("#btn_add").click(function(){
                    $.post("_p0.php",
                        {
                            cmd: "registerBookDetails",
                            newAuthor: document.getElementById('newAuthor').value,
                            newGenre: document.getElementById('newGenre').value,
                            newStatus: document.getElementById('newStatus').value

                        },
                        function(data,status){
                            //alert("Data: " + data + "\nStatus: " + status);
                            alert(data);
                        })
                })
            })
        </script>
    </body>
</html>