<html>
    <head>
        <title>📚 </title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body class="bg-color">
    <?php include "navbarManager.php"; ?>
        <div align="center">
            <br /><br /><br />
            <div class="card login">
                <div  class="container">
                    <br />
                    <form method="post" action="_p0.php">
                        <div class="form-group">
                            <label>Email</label>
                            <input name="u_email" type="email" class="form-control" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input name="u_password" type="password" class="form-control" autocomplete="off" required>
                        </div>
                        <button name="cmd" value="login" type="submit" class="btn">Login</button>
                    </form>
                    <hr />Don't have an account? <a href="form_registerUser.php"><u>Create an account</u></a>
                    <br /><br />
                </div>
            </div>
        </div>
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>