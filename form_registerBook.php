<?php 
    include_once "_p1.php";
    include_once "_session.php";
    isLoggedIn();
?>
<html>
    <head>
        <title>📚 </title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        <style>
            .hide{
                display: none;
            }
        </style>
    </head>
    <body>
        <?php include_once 'navbarManager.php'; ?>
        <div class="container">
            <div class="" style="padding: 100px">
                <form method="post" action="_p0.php">
                    <div class="form-group">
                        <label>ISBN</label>
                        <input name="bk_isbn" type="text" class="form-control" placeholder="ISBN" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input name="bk_title" type="text" class="form-control" placeholder="Title" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Author Name</label>
                        <select id="bk_author" name="bk_author" class="form-control">
                            <option value="-1">Choose Author...</option>
                            <!-- option value="-2">Other</option -->
                            <?php authorList(); ?>
                        </select>
                        <input id="bk_author2" name="bk_author2" type="text" class="form-control hide" placeholder="Author Name" autocomplete="off" disabled required>
                    </div>
                    <div class="form-group">
                        <label>Genre</label>
                        <select id="bk_genre" name="bk_genre" class="form-control">
                            <option value="-1">Choose Genre...</option>
                            <!-- option value="-2">Other</option -->
                            <?php bookGenreList(); ?>
                        </select>
                        <input id="bk_genre2" name="genre2" type="text" class="form-control hide" placeholder="Genre" autocomplete="off" disabled required>
                    </div>
                    <button type="reset" class="btn btn-danger">Reset</button>
                    <button name="cmd" value="registerBook" type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script>
        /*
            $(document).ready(function(){
                $('#bk_author').change(function(){
                    var e1 = $('#bk_author')
                    var e2 = $('#bk_author2')
                    
                    if(e1.val() == "-2"){
                        e2.fadeIn()
                        e2.prop('disabled', false)
                    }else{
                        e1.fadeIn()
                        e2.fadeOut()
                        e2.prop('disabled', true)
                    }
                })
                
                $('#bk_genre').change(function(){
                    var e1 = $('#bk_genre')
                    var e2 = $('#bk_genre2')
                    
                    if(e1.val() == "-2"){
                        e2.fadeIn()
                        e2.prop('disabled', false)
                    }else{
                        e1.fadeIn()
                        e2.fadeOut()
                        e2.prop('disabled', true)
                    }
                })
            })
        */
        </script>
    </body>
</html>