<?php

$id = $_GET['id'];
$title = $_GET['title'];
$isbn = $_GET['isbn'];

echo "<html>
<head>
    <title>📚 </title>
    <link href='assets/css/bootstrap.min.css' rel='stylesheet'>
    <link href='assets/css/style.css' rel='stylesheet'>
    
</head>
<body onload='window.print();'>
    <input id='text' type='text' value='$id' style='width:80%' hidden/>
    <br />
    <div align='' class='container card'>
        <div class='row'>&nbsp;&nbsp;&nbsp;
            <div class='col-md-5' id='qrcode' style='width:300px; height:315px; margin-top:15px;' >
                
            </div>
            <br />
            <div class='col-md'></div>
            <div class='col-md-6'>
                <br />
                <br />$title
                <br />$isbn
            </div>
        </div>

        
    
    
    </div>
    <script src='assets/js/jquery-3.3.1.min.js'></script>
    <script src='assets/js/tether.min.js'></script>
    <script src='assets/js/bootstrap.min.js'></script>
    <script src='assets/js/qrcode.min.js'></script>
    
   <script type='text/javascript'>
    var qrcode = new QRCode(document.getElementById('qrcode'), {
        width : 300,
        height : 300
    });

    function makeCode () {		
        var elText = document.getElementById('text');

        if (!elText.value) {
            alert('Input a text');
            elText.focus();
            return;
        }

        qrcode.makeCode(elText.value);
    }

    makeCode();

    $('#text').
        on('blur', function () {
            makeCode();
        }).
        on('keydown', function (e) {
            if (e.keyCode == 13) {
                makeCode();
            }
        });
    </script>
</body>
</html>
";

echo "<script>
        setTimeout(function(){
            document.location.href='list_Books.php';
        },500);
</script>";

?>