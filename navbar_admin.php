            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">📚</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="dashboard_staff.php">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="list_Books.php">Browse Books</a>
                        <a class="nav-item nav-link" href="form_registerBook.php">Add Books</a>
                        <a class="nav-item nav-link" href="form_registerBookDetails.php">Add Books Details</a>
                        <a class="nav-item nav-link" href="_p0.php?cmd=logout">Logout</a>
                    </div>
                </div>
            </nav>