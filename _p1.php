<?php
    include '_dbconnect.php';
    include '_session.php';

    $cmd = null;

    if(isset($_POST['cmd'])){
        $cmd = $_POST['cmd'];
    }else if(isset($_GET['cmd'])){
        $cmd = $_GET['cmd'];
    }else{
        //echo "Error : Command Not Found!";
    }

    if($cmd != null){
        $conn = connect();
        switch($cmd){
            case "ScanBook": ScanBook($conn); break;
            default: 
        }
    }

    
    function ScanBook($conn){
        $conn = connect();
        $cmd2 = null;

        if(isset($_POST['cmd2'])){
            $cmd2 = $_POST['cmd2'];
            //RETURN A BOOK
            if($cmd2 == 'return'){
                $bookID = $_POST['data'];
                $sql = "SELECT
                            BORROW_TABLE_ID,
                            BORROW_BOOK_ID,
                            BOOK_DETAIL_ISBN,
                            BOOK_DETAIL_NAME,
                            BOOK_AUTHOR_NAME,
                            BOOK_GENRE_NAME,
                            BOOK_STATUS_NAME,
                            BORROW_DATE_START,
                            BORROW_DATE_END,
                            BORROW_DATE_RETRUN,
                            BORROW_USER_ID
                        FROM
                            BORROW_TABLE,
                            BOOK_STATUS,
                            BOOK_DETAIL,
                            BOOK_AUTHOR,
                            BOOK_GENRE
                        WHERE
                            BORROW_BOOK_ID = BOOK_DETAIL_ID
                        AND
                            BOOK_AUTHOR_ID = BOOK_DETAIL_AUTHOR
                        AND
                            BOOK_GENRE_ID = BOOK_DETAIL_GENRE
                        AND
                            BOOK_DETAIL_STATUS = BOOK_STATUS_ID
                        AND 
                            BOOK_DETAIL_ID = $bookID
                        AND
                            BORROW_DATE_RETRUN IS null";
                $stid = executeSQL($conn,$sql);

                $row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
                $data = Array(
                    $row['BORROW_BOOK_ID'],
                    $row['BOOK_DETAIL_ISBN'],
                    $row['BOOK_DETAIL_NAME'],
                    $row['BOOK_AUTHOR_NAME'],
                    $row['BORROW_DATE_START'],
                    $row['BORROW_DATE_END'],
                    $row['BORROW_DATE_RETRUN'],
                    $row['BOOK_STATUS_NAME'],
                    $row['BORROW_TABLE_ID']
                        
                );

                if($data[7] == 'Unavailable'){
                    echo "<div style='padding: 50px'>
                    <br />
                    <div >
                        <!-- div class='row bg'>
                            <div class='col'>
                                User ID
                            </div>
                            <div class='col'>
                                1
                            </div>
                        </div>
                        <div class='row bg2'>
                            <div class='col'>
                                User Name
                            </div>
                            <div class='col'>
                                2
                            </div>
                        </div -->
                        <div class='row bg'>
                            <div class='col'>
                                Book Title
                            </div>
                            <div class='col'>
                                $data[2]
                            </div>
                        </div>
                        <div class='row bg2'>
                            <div class='col'>
                                Borrow Date
                            </div>
                            <div class='col'>
                                $data[4]
                            </div>
                        </div>
                        <div class='row bg'>
                            <div class='col'>
                                Due Date
                            </div>
                            <div class='col'>
                                $data[5]
                            </div>
                        </div><br />

                    </div>
                </div>
                <a class='btn btn-success' href='_p0.php?cmd=returnBook&borrowID=$data[8]&bookID=$data[0]'>Return </a>
                
                <a class='btn btn-info' href='tool_scanQR_return.php'>Cancel </a>
                <br /><br />";
                }else{
                    echo "<div align='center'>
                    <h3>This book not on your borrow lists.</h3>
                    </div>";
                }
                
                
            }else{
                //BORROW A BOOK
                $bookID = $_POST['data'];
                $userID = $_SESSION['userID'];
                $sql = "select 
                        BOOK_DETAIL_ID,
                        BOOK_DETAIL_ISBN,
                        BOOK_DETAIL_NAME,
                        BOOK_AUTHOR_NAME,
                        BOOK_GENRE_NAME,
                        BOOK_STATUS_NAME
                    from 
                        BOOK_DETAIL,
                        BOOK_AUTHOR,
                        BOOK_GENRE,
                        BOOK_STATUS
                    where
                        BOOK_AUTHOR_ID = BOOK_DETAIL_AUTHOR
                    and
                        BOOK_GENRE_ID = BOOK_DETAIL_GENRE
                    and
                        BOOK_STATUS_ID = BOOK_DETAIL_STATUS
                    and
                        BOOK_DETAIL_ID = $bookID";

                
                    $stid = executeSQL($conn,$sql);
                    $row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
                    $data = Array(
                        $row['BOOK_DETAIL_ID'],
                        $row['BOOK_DETAIL_ISBN'],
                        $row['BOOK_DETAIL_NAME'],
                        $row['BOOK_AUTHOR_NAME'],
                        $row['BOOK_GENRE_NAME'],
                        $row['BOOK_STATUS_NAME']
                    );
                
                    if($data[5] == 'Available'){
                    echo "<div style='padding: 50px'>
                            <br />
                            <div >
                                <!-- div class='row bg'>
                                    <div class='col'>
                                        Book ID
                                    </div>
                                    <div class='col'>
                                        $data[0]
                                    </div>
                                </div>
                                <div class='row bg2'>
                                    <div class='col'>
                                        ISBN
                                    </div>
                                    <div class='col'>
                                        $data[1]
                                    </div>
                                </div -->
                                <div class='row bg'>
                                    <div class='col'>
                                        Title
                                    </div>
                                    <div class='col'>
                                        $data[2]
                                    </div>
                                </div>
                                <div class='row bg2'>
                                    <div class='col'>
                                        Author
                                    </div>
                                    <div class='col'>
                                        $data[3]
                                    </div>
                                </div>
                                <div class='row bg'>
                                    <div class='col'>
                                        Genre
                                    </div>
                                    <div class='col'>
                                        $data[4]
                                    </div>
                                </div><br />

                            </div>
                        </div>
                        <a class='btn btn-success' href='_p0.php?cmd=borrowBook&id=$data[0]'>Borrow</a>

                        <a class='btn btn-info' href='tool_scanQR_borrow.php'>Cancel </a>
                        <br /><br />";
                }else{
                    echo "<div align='center'>
                    <h3>Someone Already Borrowed!</h3>
                    </div>";
                }

                
            }
        }
        
    }
    
    function authorList(){
        $conn = connect();
        $sql = "select * from BOOK_AUTHOR";
        $stid = executeSQL($conn,$sql);

        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
            $id = $row['BOOK_AUTHOR_ID'];
            $name = $row['BOOK_AUTHOR_NAME'];
            echo "<option value='$id'>$name</option>";
        }
        oci_close($conn);
    }

    function bookStatusList(){
        $conn = connect();
        $sql = "select * from BOOK_STATUS";
        $stid = executeSQL($conn,$sql);

        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
            $id = $row['BOOK_STATUS_ID'];
            $name = $row['BOOK_STATUS_NAME'];
            echo "<option value='$id'>$name</option>";
        }
        oci_close($conn);
    }

    function bookGenreList(){
        $conn = connect();
        $sql = "select * from BOOK_GENRE";
        $stid = executeSQL($conn,$sql);

        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
            $id = $row['BOOK_GENRE_ID'];
            $name = $row['BOOK_GENRE_NAME'];
            echo "<option value='$id'>$name</option>";
        }
        oci_close($conn);
    }

    function bookList(){
        $conn = connect();
        $sql = "select 
                    BOOK_DETAIL_ID,
                    BOOK_DETAIL_ISBN,
                    BOOK_DETAIL_NAME,
                    BOOK_AUTHOR_NAME,
                    BOOK_GENRE_NAME,
                    BOOK_STATUS_NAME
                from 
                    BOOK_DETAIL,
                    BOOK_AUTHOR,
                    BOOK_GENRE,
                    BOOK_STATUS
                where
                    BOOK_AUTHOR_ID = BOOK_DETAIL_AUTHOR
                and
                    BOOK_GENRE_ID = BOOK_DETAIL_GENRE
                and
                    BOOK_STATUS_ID = BOOK_DETAIL_STATUS
                order by
                    BOOK_DETAIL_NAME ASC";

        $stid = executeSQL($conn,$sql);

        $num = 1;
        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
            $data = Array(
                $row['BOOK_DETAIL_ID'],
                $row['BOOK_DETAIL_ISBN'],
                $row['BOOK_DETAIL_NAME'],
                $row['BOOK_AUTHOR_NAME'],
                $row['BOOK_GENRE_NAME'],
                $row['BOOK_STATUS_NAME']
            );
            echo "  <tr>
                        <th scope='row'>$num</th>
                        <td>$data[2]</td>
                        <td>$data[3]</td>
                        <td>$data[4]</td>
                        <td>$data[5]</td>
                        ";

            if($_SESSION['userType'] == 'ADM' || $_SESSION['userType'] == 'STF'){
                if($data[5] == "Available"){
                    echo "  <td>
                                <a class='btn btn-success' href='tool_generateQR.php?cmd=0&id=$data[0]&title=$data[2]&isbn=$data[1]'>Print QR</ a>
                            </td>
                        </tr>";
                }else{
                    echo "  <td>
                                <button disabled class='btn btn-success'>Print QR</ button>
                            </td>
                        </tr>";
                }
            }else{
                echo "<td></td></tr>";
            }
            $num++;
        }
        oci_close($conn);
    }


    function bookBorrowedList(){
        $conn = connect();
        $userID = $_SESSION['userID'];
        $sql = "SELECT
                    BORROW_TABLE_ID,
                    BORROW_BOOK_ID,
                    BOOK_DETAIL_ISBN,
                    BOOK_DETAIL_NAME,
                    BOOK_AUTHOR_NAME,
                    BOOK_GENRE_NAME,
                    BOOK_STATUS_NAME,
                    BORROW_DATE_START,
                    BORROW_DATE_END,
                    BORROW_DATE_RETRUN,
                    BORROW_USER_ID
                FROM
                    BORROW_TABLE,
                    BOOK_STATUS,
                    BOOK_DETAIL,
                    BOOK_AUTHOR,
                    BOOK_GENRE
                WHERE
                    BORROW_BOOK_ID = BOOK_DETAIL_ID
                AND
                    BOOK_AUTHOR_ID = BOOK_DETAIL_AUTHOR
                AND
                    BOOK_GENRE_ID = BOOK_DETAIL_GENRE
                AND
                    BOOK_DETAIL_STATUS = BOOK_STATUS_ID
                AND
                    BORROW_USER_ID = $userID
                ORDER BY
                    BORROW_DATE_RETRUN DESC";

        $stid = executeSQL($conn,$sql);

        $num = 1;
        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
            $data = Array(
                $row['BORROW_BOOK_ID'],
                $row['BOOK_DETAIL_ISBN'],
                $row['BOOK_DETAIL_NAME'],
                $row['BOOK_AUTHOR_NAME'],
                $row['BORROW_DATE_START'],
                $row['BORROW_DATE_END'],
                $row['BORROW_DATE_RETRUN'],
                $row['BOOK_STATUS_NAME'],
                $row['BORROW_TABLE_ID']
                
            );
            echo "  <tr>
                        <th scope='row'>$num</th>
                        <td>$data[2]</td>
                        <td>$data[3]</td>
                        <td>$data[4]</td>
                        <td>$data[5]</td>
                        <td>$data[6]</td>
                    </tr>";
                        
            $num++;
        }
        oci_close($conn);
    }

    //authorList();
    //bookStatusList();
    //bookGenreList();
    //bookList();
?>