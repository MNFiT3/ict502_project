<html>
    <head>
        <title>📚 </title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body class="bg-color">
    <?php include "navbarManager.php"; ?>
        <div class="container" align="center">
            <br />
            <div class="card register"style="padding: 100px">
                <form method="post" action="_p0.php">
                    <div class="form-group">
                        <input name="u_email" type="email" class="form-control" placeholder="E-Mail" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <input name="u_username" type="text" class="form-control" placeholder="Username" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <input name="u_hp" type="tel" class="form-control" placeholder="Phone Number" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <input name="u_password" type="password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <input name="u_password2" type="password" class="form-control" placeholder="Re-Type Password" required>
                    </div>
                    <button type="reset" class="btn btn-danger">Reset</button>
                    <button name="cmd" value="registerUser" type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>