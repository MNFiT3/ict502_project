<?php

    if (session_status() == PHP_SESSION_NONE){
        session_start();
    }

    function isLoggedIn(){
        if (!empty($_SESSION['userID'])){
            return true;
        }else{
            echo "<script> alert ('Must login to proceed...'); document.location.href='form_login.php';</script>";
            return false;
        }
    }
    
    function whoLoggedIn(){
        if (isLoggedIn()){
            $user->ID = $_SESSION['userID'];
            $user->Type = $_SESSION['userType'];
            //$user->

            return $user;
        }else{
            echo "Error: Invalid Session!";
            return null;
        }
    }
?>