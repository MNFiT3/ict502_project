            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">📚</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-item nav-link" href="dashboard_user.php">Home </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Browse Books
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="list_Books.php">View Books</a>
                                    <a class="dropdown-item" href="tool_scanQR_borrow.php">Borrow</a>
                                    <a class="dropdown-item" href="tool_scanQR_return.php">Return</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-item nav-link" href="list_BorrowedBooks.php">My Book Shelf</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-item nav-link" href="_p0.php?cmd=logout">Logout</a>
                            </li>
                            
                        </ul>
                </div>
            </nav>
                        
                        
                    