<?php 
    include_once "_p1.php";
    include_once "_session.php";
    isLoggedIn();
?>
<html>
    <head>
        <title>📚 </title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div>
            <?php include "navbarManager.php"; ?>
            <br />
            <div class="container">
                <div class="row">
                    <div class="col">
                        Welcome
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>