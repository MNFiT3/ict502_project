<?php
    function connect(){
        include "_config.php";

        $conn = oci_connect($DB_username,$DB_password,"localhost/XE");

        if (!$conn) {
            echo 'Failed to connect to Oracle';
        }else{
            //echo 'Succesfully connected with Oracle DB';
        }
        
        if (!$conn) {
            $e = oci_error();
        }

        return $conn;
    }

    function numRow($conn,$sql){
        $stid = executeSQL($conn,$sql);
        oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
        return oci_num_rows($stid);
    }

    function executeSQL($conn,$sql){
        $stid = oci_parse($conn, $sql);
        if (!$stid) {
            $e = oci_error($conn);
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        $r = oci_execute($stid);
        if (!$r) {
            $e = oci_error($stid);
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        return $stid;
    }
    
?>