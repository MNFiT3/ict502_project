<?php
    //include '_config.php';
    include '_dbconnect.php';
    include '_session.php';
    include '_encryption.php';
    include '_generateUUID.php';

    $cmd = null;

    if(isset($_POST['cmd'])){
        $cmd = $_POST['cmd'];
    }else if(isset($_GET['cmd'])){
        $cmd = $_GET['cmd'];
    }else{
        echo "Error : Command Not Found! " . $cmd;
    }

    if($cmd != null){
        $conn = connect();
        switch($cmd){
            case "registerUser": registerUser($conn); break;
            case "registerBook": registerBook($conn); break;
            case "registerBookDetails" : registerBookDetails($conn); break;
            case "login": login($conn); break;
            case "logout": logout($conn); break;
            case "borrowBook": borrowBook($conn); break;
            case "returnBook": returnBook($conn); break;
            default: 
        }
    }

    /*
    *   SYSTEM FUNCTIONS
    */


    function returnBook($conn){
        if(isset($_SESSION['userID'])){
            $bookID = $_GET['bookID'];
            $borrowID = $_GET['borrowID'];
            $userID = $_SESSION['userID'];
            $date = $GLOBALS["Date"];

            $sql = "UPDATE BORROW_TABLE SET BORROW_DATE_RETRUN = TO_DATE('$date', 'yyyy/mm/dd') WHERE BORROW_TABLE_ID = $borrowID";
            executeSQL($conn, $sql);
            $sql = "UPDATE BOOK_DETAIL SET BOOK_DETAIL_STATUS = 2 WHERE BOOK_DETAIL_ID = $bookID";
            executeSQL($conn, $sql);
            redirect('list_BorrowedBooks.php', 'Success!');
        }else{

        }

        oci_close($conn);

    }

    function borrowBook($conn){
        if(isset($_SESSION['userID'])){
            $bkID = $_GET['id'];
            $userID = $_SESSION['userID'];
            $date = $GLOBALS["Date"];
            $dueDate = date('Y-m-d', strtotime($date. ' + 14 days'));
            $sql = "INSERT INTO BORROW_TABLE (BORROW_TABLE_ID, BORROW_USER_ID, BORROW_BOOK_ID, BORROW_DATE_START, BORROW_DATE_END) VALUES (BORROW_TABLE_ID_SEQ.NEXTVAL, $userID, $bkID, TO_DATE('$date', 'yyyy/mm/dd'), TO_DATE('$dueDate', 'yyyy/mm/dd'))";
            executeSQL($conn, $sql);
            $sql = "UPDATE BOOK_DETAIL SET BOOK_DETAIL_STATUS = 1 WHERE BOOK_DETAIL_ID = $bkID";
            executeSQL($conn, $sql);
            redirect('list_Books.php', 'Succesfully Added!');
        }else{

        }

        oci_close($conn);
    }

    function logout($conn){
        session_destroy();
        redirect('form_login.php', 'Succesfully Logout!');
    }

    //Login
    function login($conn){
        $status = false;
        $salt = null;
        $data = Array(
            $_POST['u_email'],
            $_POST['u_password']
        );

        $sql = "SELECT * FROM USER_LIST WHERE USER_LIST_EMAIL = '$data[0]'";
        $stid = executeSQL($conn, $sql);

        if (numRow($conn, $sql) == 1){
            $row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
            $salt = $row['USER_LIST_SALT'];
            $password = $row['USER_LIST_PASSWORD'];
            
            if ($password == dec_enc('encrypt', $data[1], $salt)){
                $status = true;
            }
        }

        if(!$status){
            redirect('form_login.php', "Incorrect Email or Password!");
        }else{
            $_SESSION['userID'] = $row['USER_LIST_ID'];
            $_SESSION['userName'] = $row['USER_LIST_NAME'];
            $_SESSION['userType'] = $row['USER_LIST_TYPE'];
            echo "Login Successfull!";
            echo '<br />UserID : ' . $_SESSION['userID'];
            echo '<br />UserName : ' . $_SESSION['userName'];
            echo '<br />UserType : ' . $_SESSION['userType'];

            switch($_SESSION['userType']){
                case "USR": redirect('dashboard_user.php', null); break;
                case "STF": redirect('dashboard_staff.php', null); break;
                case "ADM": redirect('dashboard_admin.php', null); break;
                default: 
            }

            
        }

        oci_close($conn);
    }

    function registerBook($conn){
        $data =  Array(
            $_POST['bk_isbn'],
            $_POST['bk_title'],
            $_POST['bk_author'],
            $_POST['bk_genre']
        );

        $sql = "INSERT INTO BOOK_DETAIL (BOOK_DETAIL_ID, BOOK_DETAIL_ISBN, BOOK_DETAIL_NAME, BOOK_DETAIL_AUTHOR, BOOK_DETAIL_GENRE, BOOK_DETAIL_STATUS) VALUES (BOOK_DETAIL_ID_SEQ.NEXTVAL, '$data[0]', '$data[1]', $data[2], $data[3], 2)";
        executeSQL($conn, $sql);
        redirect('form_registerBook.php', 'Succesfully Added!');
        oci_close($conn);
    }

    function registerBookDetails($conn){
        $author = $_POST['newAuthor'];
        $genre = $_POST['newGenre'];
        $status = $_POST['newStatus'];
        $flag = false;
        if ($author != ''){
            $sql = "INSERT INTO BOOK_AUTHOR (BOOK_AUTHOR_ID, BOOK_AUTHOR_NAME) VALUES (BOOK_AUTHOR_ID_SEQ.NEXTVAL, '$author')";
            executeSQL($conn, $sql);
            $flag = true;
        }
        if ($genre != ''){
            $sql = "INSERT INTO BOOK_GENRE (BOOK_GENRE_ID, BOOK_GENRE_NAME) VALUES (BORROW_GENRE_ID_SEQ.NEXTVAL, '$genre')";
            executeSQL($conn, $sql);
            $flag = true;
        }
        if ($status != ''){
            $sql = "INSERT INTO BOOK_STATUS (BOOK_STATUS_ID, BOOK_STATUS_NAME) VALUES (BORROW_STATUS_ID_SEQ.NEXTVAL, '$status')";
            executeSQL($conn, $sql);
            $flag = true;
        }
        oci_close($conn);

        if(!$flag){
            echo "An error occured";
        }else{
            echo "Succesfully Added!";
        }
        
    }

    //Register user
    function registerUser($conn){
        $data = Array(
            $_POST['u_email'],
            $_POST['u_username'],
            $_POST['u_hp'],
            $_POST['u_password']
        );

        $sql = "SELECT * FROM USER_LIST WHERE USER_LIST_EMAIL = '$data[0]'";
        
        if(numRow($conn, $sql) == 0){
            //TODO: Check uniqueness value before proceed to Insert.

            $salt = rand();
            $data[3] = dec_enc('encrypt', $data[3], $salt);

            $sql = "INSERT INTO USER_LIST (USER_LIST_ID, USER_LIST_NAME, USER_LIST_EMAIL, USER_LIST_PHONE, USER_LIST_PASSWORD, USER_LIST_TYPE, USER_LIST_SALT) VALUES (USER_ID_SEQ.NEXTVAL, '$data[1]', '$data[0]', '$data[2]', '$data[3]', 'USR', '$salt')";
            executeSQL($conn, $sql);
        } 
        redirect('form_login.php', 'Succesfully Registered!');
        oci_close($conn);
    }

    /*
    * OTHER FUNCTIONS
    */

    function redirect($url, $msg){
        if ($msg != null){
            echo "<script> alert ('$msg'); document.location.href='$url';</script>";
        }else{
            echo "<script>document.location.href='$url';</script>";
        }
        
    }
?>