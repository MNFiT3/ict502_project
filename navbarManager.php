<?php
    include_once '_session.php';

    if(isset($_SESSION['userType'])){
        switch($_SESSION['userType']){
            case "USR" : include "navbar_user.php"; break;
            case "STF" : include "navbar_staff.php"; break;
            case "ADM" : include "navbar_admin.php"; break;
            default : 
        }
    }else{
        include "navbar.php";
    }
    
?>