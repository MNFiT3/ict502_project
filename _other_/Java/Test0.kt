import java.sql.DriverManager

fun main(args:Array<String>){
    try{
        Class.forName("oracle.jdbc.driver.OracleDriver")
        val con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","root")
        var stmt = con.createStatement()
        var rs = stmt.executeQuery("select * from hr.employees")

        while (rs.next()){
            println(rs.getInt(1))
        }
        con.close()
    }catch (e:Exception){
        println(e)
    }
}