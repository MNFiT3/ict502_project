<html>
<head><title>Oracle demo</title></head>
<body>
	<?php 
	$conn=oci_connect("system","root","localhost/XE");
	If (!$conn)
		echo 'Failed to connect to Oracle';
	else 
		echo 'Succesfully connected with Oracle DB';
	
	if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

// Prepare the statement
$stid = oci_parse($conn, 'SELECT * FROM hr.departments');
if (!$stid) {
    $e = oci_error($conn);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

// Perform the logic of the query
$r = oci_execute($stid);
if (!$r) {
    $e = oci_error($stid);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

// Fetch the results of the query
print "<table border='1'>\n";
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    //print $row['DEPARTMENT_ID'] . "ssss";
    print "<tr>\n";
    foreach ($row as $item) {
        print "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
    print "</tr>\n";
}

print "</table>\n";
$numRow = oci_num_rows($stid);
print $numRow . " Rows";

oci_free_statement($stid);

oci_close($conn);
?>

</body>
</html>
