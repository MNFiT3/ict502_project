<?php 
    include_once "_p1.php";
    include_once "_session.php";
    isLoggedIn();
?>
<html>
    <head>
        <title>📚 </title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        
        <script src="assets/js/adapter.min.js"></script>
        <script src="assets/js/vue.min.js"></script>
        <script src="assets/js/instascan.min.js"></script>
        
        <style>
            .bg{
                background-color: blanchedalmond;
            }
            
            .bg2{
                background-color: oldlace;
            }
        </style>
    </head>
    <body>
    <?php include "navbarManager.php"; ?>
        <div>
            <div>
                <br />
                <div class="container card">
                    <br />
                    <h3>Return</h3><hr />
                    <h4>Scan QR Code</h4>
                    <div  align="center">
                        <div class="row">
                            <div class="col">
                                <video width="200" height="200" id="preview"></video>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div id="result">
                        
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var audio = new Audio('assets/sound/scan.mp3');
                
                let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });

                scanner.addListener('scan', function (content) {
                    //alert(content)
                    
                    $.post("_p1.php",
                    {
                            cmd: "ScanBook",
                            cmd2: "return",
                            data: content

                    },
                    function(data,status){
                        audio.play();
                        document.getElementById('result').innerHTML = data;
                    })
                    
                });

                Instascan.Camera.getCameras().then(function (cameras) {
                    if (cameras.length > 0) {
                        scanner.start(cameras[0]);
                    } else {
                        console.error('No cameras found.');
                    }
                }).catch(function (e) {
                    console.error(e);
                });
            })

            </script>
    </body>
</html>